## Qdialog ##

Qdialog : a qt4 dialog program software like kdialog or zenity

## FAQ ##

see the Venenux google groups :  http://groups.google.com/group/venenuxsarisari

## Compiling ##

* cd src
* qmake src.pro
* make

qdialog will be compiled in the under directory ouside of src.

## USage ##

These some brief of the usage:

status  argumen   parameters need for specific widgiet argument.

* [+]  --calendar     <text> <height> <width> <day> <month> <year>
* [+]  --checklist    <text> <height> <width> <list height> <tag1> <item1> <status1>...
* [+]  --dselect      <directory> <height> <width>
* [+]  --fselect      <filepath> <height> <width>
* [+]  --timebox      <text> <height> <width> <hour> <minute> <second>
* [+]  --gauge        <text> <height> <width> [<percent>]
* [+]  --textbox      <file> <height> <width> <filename>
* [+]  --editbox      <file> <height> <width> <filename>
* [ ]  --tailbox      <file> <height> <width> <filename>
* [ ]  --tailboxbg    <file> <height> <width> <filename>
* [+]  --pause        <text> <height> <width> <seconds>
* [ ]  --progress     <text> <height> <width> [<maxdots> [[-]<msglen>]]
* [ ]  --progressbox  <height> <width>
* [ ]  --mixedgauge   <text> <height> <width> <percent> <tag1> <item1>...
* [+]  --form         <text> <height> <width> <form height> <label1> <l_y1> <l_x1> <item1> <i_y1> <i_x1> <flen1> <ilen1>...
* [+]  --passwordform <text> <height> <width> <form height> <label1> <l_y1> <l_x1> <item1> <i_y1> <i_x1> <flen1> <ilen1>...
* [+]  --mixedform    <text> <height> <width> <form height> <label1> <l_y1> <l_x1> <item1> <i_y1> <i_x1> <flen1> <ilen1> <itype>...
* [+]  --inputbox     <text> <height> <width> [<init>]
* [+]  --inputmenu    <text> <height> <width> <menu height> <tag1> <item1>...
* [ ]                 use persistent editor and rename button in inputmenu
* [+]  --menu         <text> <height> <width> <menu height> <tag1> <item1>...
* [+]  --msgbox       <text> <height> <width>
* [+]  --passwordbox  <text> <height> <width> [<init>]
* [+]  --radiolist    <text> <height> <width> <list height> <tag1> <item1> <status1>...
* [+]  --yesno        <text> <height> <width>
* [+]  --infobox     <text> <height> <width> [<timeout>]
* [ ]  --2inputsbox  <text> <height> <width> <label1> <init1> <label2> <init2>
* [ ]  --3inputsbox  <text> <height> <width> <label1> <init1> <label2> <init2> <label3> <init3>
* [ ]  --combobox    <text> <height> <width> <item1> ... <itemN>
* [ ]  --rangebox    <text> <height> <width> <min value> <max value> [<default value>]
* [ ]  --2rangesbox  <text> <height> <width> <label1> <min1> <max1> <def1> <label2> <min2> <max2> <def2>
* [ ]  --3rangesbox  <text> <height> <width> <label1> <min1> <max1> <def1> <label2> <min2> <max2> <def2> <label3> <min3> <max3> <def3>
* [ ]  --spinbox     <text> <height> <width> <min> <max> <def> <label>
* [ ]  --2spinsbox   <text> <height> <width> <min1> <max1> <def1> <label1> <min2> <max2> <def2> <label2>
* [ ]  --3spinsbox   <text> <height> <width> <text> <height> <width> <min1> <max1> <def1> <label1> <min2> <max2> <def2> <label2> <min3> <max3> <def3> <label3>
* [ ]  --logbox      <file> <height> <width>
* [ ]  --buildlist   <text> <height> <width> <list height> <tag1> <item1> <status1> {<help1>}...
* [ ]  --treeview    <text> <height> <width> <list height> <tag1> <item1> <status1> <item_depth1> {<help1>}...
* [ ]  --colorsel    <text> <height> <width>
* [ ]  --fontsel     <font name> <height> <width>
 
## Author ## 

* Copyright (C) 2009 Evgeniy V. Sergeev. <evgeniy.sergeev@gmail.com> All rights reserved. Original author.
* Copyright (C) 2006-2007 Sergey A. Sukiyazov.  <sukiyazov@mail.ru>. 3rt party code implementation
* Copyright (C) 2014-2015 PICCORO Lenz McKAY. <mckaygerhard@gmail.com> minimal improvements for venenux.

## LICENCED GPL ##

licence as GPL see copying file ain root of project.
